//Antonio Lia
//09/06/2018
//CSE 002-310
//This cyclometer program records time elapsed in seconds the number of rotations a front wheel has. 
//For two trips, the program will print number of minutes for each trip, the number of counts for each trip, 
//the distance of each trip in miles, and the distance of the two trips combined.

public class Cyclometer{
  //main method required for Java
  public static void main(String[] args){

  //our input data  
  int secsTrip1=480; //the amount of time for trip one in seconds 
  int secsTrip2=3220; //the amount of time for trip two in seconds 
  int countsTrip1=1561; //the amount of rotations for trip one 
  int countsTrip2=9037; //the amount of rotations for trip two 
  double wheelDiameter=27.0; //the diameter of the wheel
  double PI=3.14159, //used for the Pi constant 
  feetPerMile=5280, //indicates the amount of feet in a mile
  inchesPerFoot=12, //indicates the amount of inches in a foot
  secondsPerMinute=60; //indicates the number of seconds in a minutes
  double distanceTrip1, distanceTrip2,totalDistance; //sums the two trips to get the total distance
  
  //prints out the minutes and rotations for each trip
  System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
  System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
  
  //The program below gives value to the two distances by calculating the distance in inches
  //It then converts the distance from inches to feet and then feet to miles
  //The two trips are then added together to give the total distance of the two trips in miles  
  distanceTrip1=countsTrip1*wheelDiameter*PI;
  //Above gives distance in inches
  //(for each count, a rotation of the wheel travels
  //the diameter in inches times PI)
	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	totalDistance=distanceTrip1+distanceTrip2; 
    
  //Print out the output data.
  System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");
  
  }
}