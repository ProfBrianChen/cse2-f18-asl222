//////////////
// CSE 002 Welcome Class
////////
public class WelcomeClass{
  
public static void main(String args[]){
  //prints out my welcome message
  System.out.println("-----------");
  System.out.println("| WELCOME |");
  System.out.println("-----------");
  System.out.println(" ^  ^  ^  ^  ^  ^");
  System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\");
  System.out.println("<-A--S--L--2--2--2->");
  System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /");
  System.out.println("v  v  v  v  v  v");

}
}