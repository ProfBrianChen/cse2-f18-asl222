//Antonio Lia
//Lab05
//The following program will use user inputs
//to determine the details of a course they are taking.

import java.util.Scanner;

public class UserInput{
  public static void main(String[] args){
    Scanner myScanner= new Scanner(System.in);//creates the scanner
    
    int courseNum=0;//initializes course number
    String departmentName="null";// initializes name of department
    int timesPerWeek=0;//initializes how many times a week the class meets
    String timeOfDay="null";//initializes the minute of the day you have a class
    String instructorName="null";//initializes the name of the instructor
    int numOfStudents=0;//initializes the number of students in a class room
    boolean correctInt=true;
    boolean correctString=true;
    boolean repeat=true;
    
    System.out.println("Enter your Course Number:");
    correctInt=myScanner.hasNextInt();
    
   while(repeat==true){
      if(correctInt){
        repeat=false;
        courseNum=myScanner.nextInt();
      
      //System.out.println(courseNum);
     }
    else {
      System.out.println("Invalid value for your course number.");
      myScanner.next();
      System.out.println("Enter your Course Number:");
      correctInt=myScanner.hasNextInt();
     }
   }
    repeat=true;
    System.out.println("Enter your Department Name:");
    correctString=myScanner.hasNext();
    
    while(repeat==true){
      if(correctString){
        repeat=false;
        departmentName=myScanner.next();
      
      //System.out.println(courseNum);
     }
    else {
      System.out.println("Invalid value for your Departmentepartment Name.");
      myScanner.next();
      System.out.println("Enter your Department Name:");
      correctString=myScanner.hasNext();
     }
   }
    repeat=true;
    System.out.println("Enter the number of times the course meets a week:");
    correctInt=myScanner.hasNextInt();
    
   while(repeat==true){
      if(correctInt){
        repeat=false;
        timesPerWeek=myScanner.nextInt();
      
      //System.out.println(courseNum);
     }
    else {
      System.out.println("Invalid value for your times per week.");
      myScanner.next();
      System.out.println("Enter the number of times the course meets a week:");
      correctInt=myScanner.hasNextInt();
     }
   }
    
    repeat=true;
    System.out.println("Enter the time of day the course meets:");
    correctString=myScanner.hasNext();
    
   while(repeat==true){
      if(correctString){
        repeat=false;
        timeOfDay=myScanner.next();
      
      //System.out.println(courseNum);
     }
    else {
      System.out.println("Invalid value for your time of day.");
      myScanner.next();
      System.out.println("Enter the time of day the course meets:");
      correctString=myScanner.hasNext();
     }
   }
    
   repeat=true;
    System.out.println("Enter your instructor's name:");
    correctString=myScanner.hasNext();
    
   while(repeat==true){
      if(correctString){
        repeat=false;
        instructorName=myScanner.next();
      
      //System.out.println(courseNum);
     }
    else {
      System.out.println("Invalid value for your instructor's name.");
      myScanner.next();
      System.out.println("Enter your instructor's name:");
      correctString=myScanner.hasNext();
     }
   }
    
    repeat=true;
    System.out.println("Enter the number of students in your class:");
    correctInt=myScanner.hasNextInt();
    
   while(repeat==true){
      if(correctInt){
        repeat=false;
        numOfStudents=myScanner.nextInt();
      
      //System.out.println(courseNum);
     }
    else {
      System.out.println("Invalid value for the number of students in your class.");
      myScanner.next();
      System.out.println("Enter the number of students in your class:");
      correctInt=myScanner.hasNextInt();
     }
   }
    
  }
}