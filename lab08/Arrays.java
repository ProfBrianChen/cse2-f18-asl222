//Antonio Lia
//This program creates two arrays that input 
//random numbers from 0-99. The other array
//counts how many times the digit occurs.counts

import java.util.*;

public class Arrays{
  public static void main(String[] args){
  Random randGen=new Random();//creates the random generator
  
  int[] firstArray = new int[100];//creates the first array
  int[] secondArray = new int[100];//creates the second array
  int count=0;//initializes the variable
  int checkInt=0;//initializes the other variable
    
  
  System.out.print("Array 1 holds the following integers: ");//prints this statement
  
  //inputs random numbers from 0-99 at the length of 100 inputs
  for(int i=0; i<firstArray.length; i++){
    firstArray[i]=(int)(Math.random()*99);
    System.out.print(firstArray[i]+" "); 
    secondArray[i]=i;
    }
    
  System.out.println("");  
   
  //counts how many times a number occurs  
  for(checkInt=0; checkInt<secondArray.length; checkInt++){
    for(int j=0; j<secondArray.length; j++){
        if(firstArray[j]==checkInt){
            count++;
          }
      }
       
        
            System.out.println(checkInt+ " occurs " +count+ " times.");
          
   count=0;
    }
  }
}

