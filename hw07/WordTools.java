//Antonio Lia
//11.01.2018
//This program uses multiple methods to 
//take an input user text and make edits
//or determine values from a print menu. 

import java.util.*;

public class WordTools{
  public static void main(String[] args){
    Scanner myScanner=new Scanner(System.in);//makes the scanner
    String textInput=sampleText();//gives value to the user input
    System.out.print(textInput);//prints the user input
    char letterInput='a';//initializes a character
    
    //switch statement that determines what happens when a button is pressed
    while(letterInput != 'q'){
      letterInput=printMenu(textInput);
    switch(letterInput){
      case 'q' : System.out.println("Quit.");
          System.exit(0);
        break;
      case 'c' : System.out.println("Number of non-whitespace characters:");
        int runC=getNumOfNonWSCharacters(textInput);
        break;
      case 'w' : //System.out.println();
        int runW=getNumOfWords(textInput);
        break;
      case 'f' : System.out.println("Find text:");
        System.out.println("Enter a word to be found: ");
        String findText=myScanner.next();
        int runF=findText(textInput, findText);
        break;
      case 's' : System.out.println("Shorten spaces: ");
        String spaceTextInput=shortenSpace(textInput);
        System.out.println("Edited string is " +spaceTextInput);
        break;
      case 'r' : System.out.println("Replace all !'s: ");
        String changedTextInput=replaceExclamation(textInput);
        System.out.println("Edited string is " +changedTextInput);
        break;
      default : System.out.println("Invalid input.");    
        letterInput=myScanner.nextLine().charAt(0);
      }
    }
  }
  
  //method that creates the user text input
  public static String sampleText(){
    Scanner myScanner=new Scanner(System.in);
    System.out.println("Enter a sample text:");
    String textInput=myScanner.nextLine();
    System.out.println("You entered: " + textInput);
    return textInput;
  }
  
  //method that creates the print menu for the user
  public static char printMenu(String textInput){
     Scanner myScanner=new Scanner(System.in);
     System.out.println("MENU");
     System.out.println("c - Number of non-whitepace characters");
     System.out.println("w - Number of words");
     System.out.println("f - Find text");
     System.out.println("r - Replace all !'s");
     System.out.println("s - Shorten spaces");
     System.out.println("q - Quit");
     System.out.println("Choose an option: ");
     char letterInput=myScanner.nextLine().charAt(0);
    return letterInput;
  }
  
  //method that determines how many characters are in a statement
  public static int getNumOfNonWSCharacters(String textInput){
    int whiteSpaces=0;
    for(int x=0; x<textInput.length(); x++ ){
      if(textInput.charAt(x)== ' '){
        whiteSpaces++;
      }
    }
    int nonWhiteSpaces=textInput.length()-whiteSpaces;
    System.out.println(nonWhiteSpaces);
    return nonWhiteSpaces;
  }
  
  //determines the number of words in a statement
  public static int getNumOfWords(String textInput){
    int words=0;
    for(int x=0; x<textInput.length(); x++){
      if(textInput.charAt(x) != ' '){
        words++;
      }
      while(textInput.charAt(x) != ' ' && x<textInput.length()-1){
        x++;
      }
    }
    System.out.println("Number of words: " + words);
    return words;
  }
  
  //finds a word input by the user
  public static int findText(String textInput, String textIWant){
    int count  = textInput.replaceAll("[^" + textIWant + "]", "").length() / textIWant.length();//theory came from stack overflow
		if(count>0){
		  System.out.println(textIWant+" occurs "+count+" times in "+textInput);
		}
		else{
		  System.out.println(textIWant+" didn't occur in "+textInput);
		}
	  return count;
  }
  
  //replaces exclamation points with a period
  public static String replaceExclamation(String textInput){  
    String changedTextInput="";
    for(int x=0; x<textInput.length(); x++){
      if(textInput.charAt(x)=='!'){
        changedTextInput=changedTextInput+'.';
      }
      else{
        changedTextInput=changedTextInput+textInput.charAt(x);
      }
    }
    return changedTextInput;
    
  }
  
  //reduces multiple spaces to one space 
  public static String shortenSpace(String textInput){
    String spaceTextInput=textInput.replaceAll("\\s{2,}", " ");//inside the parentheses cane from stack overflow
    return spaceTextInput;
  }
}