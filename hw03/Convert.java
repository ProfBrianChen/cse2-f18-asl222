import java.util.Scanner;//imports the scanner function

//Antonio Lia
//9/17/18
//The program will calculate the number of acres of land 
//affected by hurricane precipitation and how many inches of 
//rain were dropped on average



public class Convert{
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner (System.in);//adds a new scanner
    System.out.print("Enter the affected area in acres: ");//asks the user to input the area of rain in acres
    double affectedArea=myScanner.nextDouble();//allows the user to input the area
    
    System.out.print("Enter the rainfall in the affected area: ");//asks the user to input the inches of rainfall
    double inchesOfRainfall=myScanner.nextDouble();//allows user to input inches of rain
    
    double acreInchesOfRain=affectedArea*inchesOfRainfall;//converts the two numbers into acre-inches
    double cubicMilesOfRain=acreInchesOfRain/40550399.58713084;//converts acre-inches into cubic miles
    System.out.print(cubicMilesOfRain+" cubic miles");//prints out cubic miles of rain
    
  }
}