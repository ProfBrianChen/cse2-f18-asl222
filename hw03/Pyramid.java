import java.util.Scanner;
//Antonio Lis
//9/17/18
//
//The program prompts the user 
//for the dimensions of a pyramid and 
//returns the volume inside the pyramid
//

public class Pyramid{
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner (System.in);//creates new scanner
    System.out.print("The square side of the pyramid is (input length): ");//asks user to input a length of the side of the base
    double baseLength=myScanner.nextDouble();//allows user to input the base length
    
    System.out.print("The height of the pyramid is (input height): ");//asks the user for the height of the pyramid
    double pyramidHeight=myScanner.nextDouble();//allows user to input the height
   
    double areaOfBase=Math.pow(baseLength, 2);//gets the area of the base
    double volumeOfPyramid=areaOfBase*pyramidHeight/3;//calculates the volume of the pyramid
    System.out.print("The volume inside the pyramid is: " +volumeOfPyramid);//displays the volume of the pyramid
    
  }
}
