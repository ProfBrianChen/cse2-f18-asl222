// Antonio Lia
// 09/11/2018
// CSE-002
//The following will calculate the prices and sales tax of
//belts, pants, and shirts. 
//

public class Arithmetic{
  public static void main(String args[]){
    
//Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltPrice = 33.99;

//the tax rate
double paSalesTax = 0.06;

//Total cost for pants
double totalCostPants;

//Total cost for belts    
double totalCostBelts;
 
//Total cost for shirts  
double totalCostShirts;
    
//Sales tax for pants   
double salesTaxPants;

//Sales tax for belts    
double salesTaxBelts;
 
//Sales tax for shirts    
double salesTaxShirts;
  
//Cost of items before tax    
double totalCostBefTax;
    
//Sales tax of all the items    
double totalSalesTax;
    
//Total cost after tax    
double totalCostAftTax; 
    
totalCostPants=numPants*pantsPrice;
totalCostBelts=numBelts*beltPrice;    
totalCostShirts=numShirts*shirtPrice;    
salesTaxPants=(int)((totalCostPants*paSalesTax)*100)/100.0;
salesTaxBelts=(int)(totalCostBelts*paSalesTax*100)/100.0;
salesTaxShirts=(int)(totalCostShirts*paSalesTax*100)/100.0;
totalCostBefTax=totalCostPants+totalCostShirts+totalCostBelts;
totalSalesTax=salesTaxPants+salesTaxBelts+salesTaxShirts;
totalCostAftTax=totalCostBefTax+totalSalesTax;    
 
System.out.println("Total Cost for Pants: $" +totalCostPants);
    System.out.println("Sales Tax for Pants: $"+ salesTaxPants);
System.out.println("Total Cost for Belts: $" +totalCostBelts);
    System.out.println("Sales Tax for Belts: $" +salesTaxBelts);
System.out.println("Totak Cost for Shirts: $"+totalCostShirts);
    System.out.println("Sales Tax for Shirts: $"+salesTaxShirts);
    System.out.println("Total Cost Before Tax: $"+totalCostBefTax);
    System.out.println("Sales Tax of All Items: $"+totalSalesTax);
    System.out.println("Total Cost After Tax: $"+totalCostAftTax);
    
  }
}