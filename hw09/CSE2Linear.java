//Antonio Lia
//This program allows the user to 
//create inputs for an array, scramble the array,
//and search the array through linear or binary search.


import java.util.*;

public class CSE2Linear{
  public static void main(String[] args){
    Scanner myScan = new Scanner(System.in);//declares the scanner
    Random randGen=new Random();//declares random generator
    
    int[] userArray = new int[15];//makes an array
    int userInput = 0;//declares user input
    
    //makes sure input is valid
    for(int i = 0; i<15; i++ ){
      System.out.println("Enter an integer from 0 to 100: ");
      while(!myScan.hasNextInt()){
            myScan.next();
            System.out.println("Not an integer. Try Again.");
          }
      userInput=myScan.nextInt();
      
      while(userInput<0 || userInput>100){
            System.out.println("Not within bound. Try Again.");
            userInput=myScan.nextInt();
          }
        
        
        userArray[i] = userInput;
     }
     
     printArray(userArray);//uses method to print array
     binarySearch(userArray);//searches through using binary search
     scramble(userArray);//scrambles the array
     printArray(userArray);//prints the scrambled array
     linearSearch(userArray);//searched linearly
  }
  
  //method that prints the array
  public static void printArray(int[] arrayOne){
      for(int j=0; j<arrayOne.length ; j++) {
       System.out.print(arrayOne[j]+ " ");
     }
    System.out.println();
    }
  
  //method that scrambles the array
  public static void scramble(int[] arrayTwo){
     for(int k=0; k<60; k++){
       int randNumOne = (int)(Math.random()*(arrayTwo.length));
       int randNumTwo = (int)(Math.random()*(arrayTwo.length));
       int temp = arrayTwo[randNumOne];
       arrayTwo[randNumOne] = arrayTwo[randNumTwo];
       arrayTwo[randNumTwo] = temp;
     }
    System.out.println("Scrambled: ");
  }
  
  //method that incorporates linear search
  public static void linearSearch(int[] arrayThree){
     Scanner scan = new Scanner(System.in);
     System.out.println("Enter a value to search: ");
     int input = scan.nextInt();
     boolean found=false;
     int x=0;
     while(x<arrayThree.length || found==false){
       if(arrayThree[x]==input){
         found=true;
         System.out.println(input+ " is in the array with "+(x+1)+" iterations.");
         break;
       }
       x++;
      if(x==arrayThree.length && found==false){
        System.out.println(input+ " is not in the array with "+arrayThree.length+" iterations.");
        break;
      } 
     }
    
  }
  
  //method that incorporates binary search
  public static void binarySearch(int[] arrayFour){
     Scanner scan = new Scanner(System.in);
     System.out.println("Enter a value to search: ");
     int input = scan.nextInt();
     boolean found = false;
     int low=0;
     int high=arrayFour.length-1;
     int count=0;
     while(high>=low){
       int mid = (low + high)/2;
       if (arrayFour[mid]==input){
         found=true;
         count++;
         System.out.println(input+ " is in the array with "+count+" iterations.");
         break;
       }
       else if(arrayFour[mid]<input){
         low=mid+1;
       }
       else if(arrayFour[mid]>input){
         high=mid-1;
       }
       count++;
       if(found==false && high<low){
         System.out.println(input+ " is not in the array with "+count+" iterations.");
         break;
       }
     }
  }
   
}