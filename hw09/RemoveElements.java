//Antonio Lia
//This program will remove an element
//given the index or the value.
import java.util.*;

public class RemoveElements{
  public static void main(String [] arg){
    
	Scanner scan=new Scanner(System.in);//scanner is declared
  Random randGen=new Random();//random is declared
    
  int num[]=new int[10];//creates an array
  int newArray1[];//creates array
  int newArray2[];//creates array
  int target;//declares target
	String answer="";//variable for user input
  int index = 0;//declares index
    
  //do-while loop that allows program to run until user says otherwise  
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
    while(scan.hasNextInt()){
      if(scan.hasNextInt()){
        index=scan.nextInt();
        if(index<0 || index>=num.length){
          System.out.println("Out of bounds. Try again.");
          scan.nextLine();
        }
        else{
          System.out.println("Index "+index+" is removed.");
          break;
        }
      }
            
    }
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
    System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  //lists out the array
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
  
  //randomly inputs values in an array
  public static int[] randomInput(){
    int[] arrayOne = new int[10];
    for(int i=0; i<arrayOne.length; i++){
      arrayOne[i]=(int)(Math.random()*10);
    }
    return arrayOne;
  }
  
  //deletes an index from the array
  public static int [] delete(int[] list, int pos){
    int count=0;
    int[] arrayOne = new int[list.length-1];
    for(int k=0; k<list.length; k++){
      if(k==pos){
        continue;
      }  
     arrayOne[count++]=list[k];
    }
    return arrayOne;
  }
  
  //removes values from the original array
  public static int [] remove(int[] list, int target){
    int count=0;
    for(int x=0; x<list.length; x++){
      if(list[x]==target){
        count++;
      }
    }
    if(count==0){
      System.out.println("Item not found.");
      return list;
    }
    else {
      System.out.println("Element "+target+" was found.");
    }
    int[] arrayThree = new int[list.length-count];
    int count2=0;
    for(int t=0; t<list.length; t++){
      if(list[t]!=target){
      arrayThree[count2]=list[t];
      count2++;
      }
    }
    return arrayThree;
  }
  
}
