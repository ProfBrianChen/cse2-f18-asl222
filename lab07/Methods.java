//Antonio Lia
//10.25.18
//This program uses methods 
//to make a mad lib for the user
import java.util.Scanner;
import java.util.Random;

public class Methods{
  
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    int check=1;
   
    
      while(check==1){
        System.out.println("Enter 1 if you would like a sentence. If not, enter any other number.");
        check=myScanner.nextInt();
        String subject=firstSentence();//gives value to the first sentence method
        secondSentence(subject);//keeps the subject from the first sentence for the second sentence
        }
    System.out.println("Goodbye!");
  }
  
  //creates the first sentence from different generators
  public static String firstSentence(){
    String subject=nounSubGenerator();
    
    System.out.println("The " + adjGenerator() + subject + verbGenerator() + "the " + 
                     adjGenerator() +  nounObjGenerator() + "." );
    
    return subject;
  }
  
  //creates a second sentence from the first sentence subject and randomly generated values
  public static void secondSentence(String subject){
    System.out.println("This " + subject + "was very " + adjGenerator() + "to the " + adjGenerator() + nounObjGenerator());
    System.out.println("It used a " +nounObjGenerator() + " when it "+ verbGenerator() + nounObjGenerator() + " at the "
                       + adjGenerator() + nounObjGenerator());
    System.out.println("That " + subject + verbGenerator()+ "its " + nounObjGenerator());
  }
  
  //generates a random adjective
  public static String adjGenerator(){
     Random randGen=new Random();
     int num=(int)(Math.random()*9);
     String randAdj="null";
    
     switch(num){
      case 0: randAdj="fat ";
        break;
      case 1: randAdj="old ";
        break;
      case 2: randAdj="lazy ";
        break;
      case 3: randAdj="new "; 
        break;
      case 4: randAdj="fun ";
        break;
      case 5: randAdj="happy ";
        break;
      case 6: randAdj="nice ";
        break;
      case 7: randAdj="cute ";
        break;
      case 8: randAdj="mean ";
        break;
      case 9: randAdj="energetic ";
        break;
     }
    return randAdj;
  }
  
  //generates a random subject noun
  public static String nounSubGenerator(){
     Random randGen=new Random();
     int num=(int)(Math.random()*9);
     String randNounSub="null";
    
     switch(num){
      case 0: randNounSub="dog ";
        break;
      case 1: randNounSub="cat ";
        break;
      case 2: randNounSub="man ";
        break;
      case 3: randNounSub="woman ";
        break;
      case 4: randNounSub="mouse ";
        break;
      case 5: randNounSub="bear ";
        break;
      case 6: randNounSub="wolf ";
        break;
      case 7: randNounSub="squirrel ";
        break;
      case 8: randNounSub="fish ";
        break;
      case 9: randNounSub="bird ";
        break;
     }
    return randNounSub;
  }
  
  // generates a random verb
  public static String verbGenerator(){
     Random randGen=new Random();
     int num=(int)(Math.random()*9);
     String randVerb="null";
    
     switch(num){
      case 0: randVerb="ate ";
        break;
      case 1: randVerb="laughed at ";
        break;
      case 2: randVerb="walked by ";
        break;
      case 3: randVerb="ran past ";
        break;
      case 4: randVerb="jumped on ";
        break;
      case 5: randVerb="waved to ";
        break;
      case 6: randVerb="bit ";
        break;
      case 7: randVerb="grabbed ";
        break;
      case 8: randVerb="hit ";
        break;
      case 9: randVerb="watched ";
        break;
     }
    return randVerb;
  }
  
  //generates a random object noun
  public static String nounObjGenerator(){
     Random randGen=new Random();
     int num=(int)(Math.random()*9);
     String randNounObj="null";
    
     switch(num){
      case 0: randNounObj="flower";
        break;
      case 1: randNounObj="cookie";
        break;
      case 2: randNounObj="car";
        break;
      case 3: randNounObj="bread";
        break;
      case 4: randNounObj="kite";
        break;
      case 5: randNounObj="truck";
        break;
      case 6: randNounObj="pumpkin";
        break;
      case 7: randNounObj="helicopter";
        break;
      case 8: randNounObj="hammer";
        break;
      case 9: randNounObj="television";
        break;
     }
    return randNounObj;
  }
  
}