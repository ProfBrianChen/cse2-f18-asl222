//Antonio Lia
//This program takes an array of a certain length. 
//It then makes a copy of the array and reverses said array.
import java.util.*;

public class ArraysAndMethods{
  public static void main(String[] args){
    int[] array0 = {0, 1, 2, 3, 4, 5, 6, 7};
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    
    System.out.println("Original Array:");
    print(array0);
    System.out.println();
    
    inverter(array0);
    print(array0);
    inverter2(array1);
    print(array1);
    int[] array3 = inverter2(array2);
    print(array3);
  }
  
  public static int[] copy(int[] numArray){
    int[] copiedArray = new int[numArray.length];
    for(int i = 0; i<numArray.length; i++){
      copiedArray[i] = numArray [i]; 
    }
    return copiedArray;
  }
  
  public static void print(int[] numArray){
    for(int i = 0; i<numArray.length; i++){
      System.out.print(numArray[i]+ ", ");
    }
    System.out.println();
  }
  
  public static void inverter(int[] numArray){
    int count=numArray.length-1;
    for(int j = 0; j<numArray.length/2; j++){
      int temp = numArray[j];
      numArray[j]=numArray[count];
      numArray[count]=temp;      
      count--;
    }
  }
  
  public static int[] inverter2(int[] numArray){
    int[] copiedArray = copy(numArray);
    int count=numArray.length-1;
    for(int j = 0; j<numArray.length/2; j++){
      int temp = copiedArray[j];
      copiedArray[j]=copiedArray[count];
      copiedArray[count]=temp;      
      count--;
    }
    return copiedArray;
  }
  
}