//Antonio Lia

import java.util.*;
public class Shuffling{ 
  public static void main(String[] args) { 
    Scanner scan = new Scanner(System.in); 
    Random randGen=new Random();
    
    //suits club, heart, spade or diamond 
    String[] suitNames={"C","H","S","D"};    
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52]; 
    String[] hand = new String[5]; 
    int numCards = 5; 
    int again = 1; 
    int index = 51;
    
    for (int i=0; i<52; i++){ 
      cards[i]=rankNames[i%13]+suitNames[i/13]; 
      System.out.print(cards[i]+" "); 
    } 
    
    System.out.println();
    printArray(cards); 
    shuffle(cards); 
    printArray(cards); 
    
   // System.out.println("Enter how many cards you would like in your hand:");
   // int numCards = scan.nextInt();
    
    while(again == 1){ 
      hand = getHand(cards,index,numCards); 
      printArray(hand);
      //getHand(cards,index,numCards);
      index = index - numCards;
      System.out.println("Enter a 1 if you want another hand drawn"); 
      again = scan.nextInt(); 
    }  
  }
  
  public static void printArray(String[] arrayOne){
      for(int j=0; j<arrayOne.length ; j++) {
       System.out.print(arrayOne[j]+ " ");
     }
    System.out.println();
    }
  
  public static void shuffle(String[] arrayTwo){
      System.out.println("Shuffled");
      int shuffler=(int)((Math.random()*100)+50);
      for(int k = 0; k<=shuffler; k++){
         int randNumOne = (int)(Math.random()*51);
         int randNumTwo = (int)(Math.random()*51);
         String temp = arrayTwo[randNumOne];
         arrayTwo[randNumOne] = arrayTwo[randNumTwo];
         arrayTwo[randNumTwo] = temp;
        }
       System.out.println();
    }
  
  public static String[] getHand(String[] arrayThree, int numOne, int numTwo){
       System.out.println("Hand"); 
       //int count = (int)(Math.random()*51);
       int count = 0;
       for(int x = 0; x<numTwo; x++){
           //String[] arrayFour = new String[numOne-x]; 
           arrayThree[0]=arrayThree[numOne-x];
           count++;
          }
      return arrayThree;
    }
}
