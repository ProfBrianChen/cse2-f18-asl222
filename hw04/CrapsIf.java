//Antonio Lia
//9.25.2018
//Homework 4: CrapsIf
//The following program will use if statements
//to simulate the casino game "craps". 

import java.util.Scanner;
import java.util.Random;

public class CrapsIf{
  public static void main(String[] args){
       Random randGen=new Random();//initiates random value
      Scanner myScanner=new Scanner(System.in);//creates new scanner
      int dice1=0;//variable for dice 1
      int dice2=0;//variable for dice 2
      String score="null";//gives value to what the string score is
    System.out.println("Would you like to manually or randomly select your dice? Type  1 for manual. Type 2 for random.");//asks if manual or random
    int diceAnswer=myScanner.nextInt();//allows user to answer
    
    //if the user answers 1
    if(diceAnswer==1){
      System.out.println("Enter your number for the first dice:");
      dice1=myScanner.nextInt();
      System.out.println("Enter your number for the second dice:");
      dice2=myScanner.nextInt();
    }
    
    //if the user answers 2
    else if(diceAnswer==2){
    
    dice1=(int) ((Math.random()*6)+1);
    dice2=(int) ((Math.random()*6)+1);
    }
      //if statements for all dice combinations
      if(dice1==1 && dice2==1){
          score="SnakeEyes";
     }
      if((dice1==1 && dice2==2) || (dice1==2 && dice2==1)){
          score="Ace Deuce";
     }
      if((dice1==1 && dice2==3) || (dice1==3 && dice2==1)){
          score="Easy Four";
     }
      if((dice1==1 && dice2==4) || (dice1==2 && dice2==3) || (dice1==3 && dice2==2) || (dice1==4 && dice2==1)){
          score="Fever Five";
     }
      if((dice1==1 && dice2==5) || (dice1==2 && dice2==4) || (dice1==4 && dice2==2) || (dice1==5 && dice2==1)){
          score="Easy Six";
     }
      if((dice1==1 && dice2==6) || (dice1==2 && dice2==5) || (dice1==3 && dice2==4) || (dice1==4 && dice2==3) || (dice1==5 && dice2==2) || (dice1==6 && dice2==1)){
          score="Seven Out";
     }
      if(dice1==2 && dice2==2){
          score="Hard Four";
     }
      if((dice1==2 && dice2==6) || (dice1==3 && dice2==5) || (dice1==5 && dice2==3) || (dice1==6 && dice2==2)){
          score="Easy Eight";
     }
      if(dice1==3 && dice2==3){
          score="Hard Six";
     }
      if((dice1==3 && dice2==6) || (dice1==4 && dice2==5) || (dice1==5 && dice2==4) || (dice1==6 && dice2==3)){
          score="Nine";
     }
      if(dice1==4 && dice2==4){
          score="Hard Eight";
     }
      if((dice1==4 && dice2==6) || (dice1==6 && dice2==4)){
          score="Easy Ten";
     }
      
      if(dice1==5 && dice2==5){
          score="Hard Ten";
     }
      if((dice1==6 && dice2==5) || (dice1==5 && dice2==6)){
          score="Yo-leven";
     }
      if(dice1==6 && dice2==6){
          score="Box Cars";
     }
    
    //prints out the dice rolled and the score  
    System.out.println("Your numbers were " +dice1+ " and " +dice2+ ".");
    System.out.println(score);
      
  }
  
}