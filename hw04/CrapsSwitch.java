//Antonio Lia
//9.25.18
//This program plays the game Craps
//and is implemented through switch statements.

import java.util.Random;
import java.util.Scanner;

public class CrapsSwitch{
  public static void main(String[] args){
    
    Random randGen=new Random();//initiates random value
      Scanner myScanner=new Scanner(System.in);//creates new scanner
      int dice1=0;//variable for dice 1
      int dice2=0;//variable for dice 2
      String score="null";//gives value to what the string score is
    System.out.println("Would you like to manually or randomly select your dice? Type  1 for manual. Type 2 for random.");//asks if manual or random
    int diceAnswer=myScanner.nextInt();//allows user to answer 
    
    //switch statement to determine if manual or random
    switch(diceAnswer){
       case 1: System.out.println("Enter your number for the first dice:");
               dice1=myScanner.nextInt();
               System.out.println("Enter your number for the second dice:");
               dice2=myScanner.nextInt();
        break;
      case 2:  dice1=(int) ((Math.random()*6)+1);
               dice2=(int) ((Math.random()*6)+1);
        break;
        
    }
    //if dice one is 1
    switch(dice1){
     case 1: switch(dice2){ //gives the dice 2 from 1 to 6
       case 1: score = "Snake Eye";
         break;
       case 2: score = "Ace Deuce";
         break;
       case 3: score = "Easy Four";
         break;
       case 4: score = "Fever Five";
         break;
       case 5: score = "Easy Six";
         break;
       case 6: score = "Seven Out";
         break;
     
     }
   }
    //if dice one is 2
    switch(dice1){
      case 2: switch(dice2){
        case 1: score="Ace Deuce";
          break;
        case 2: score="Hard Four";
          break;
        case 3: score="Fever Five";
          break;
        case 4: score="Easy Six"; 
          break;
        case 5: score="Seven Out";
          break;
        case 6: score="Easy Eight"; 
          break;
      }
    }
    //if dice one is 3
    switch(dice1){
      case 3: switch(dice2){
         case 1: score="Easy Four";
           break;
         case 2: score="Fever Five"; 
           break;
         case 3: score="Hard Six";
           break;
         case 4: score="Seven Out"; 
           break;
         case 5: score="Easy Eight";
           break;
         case 6: score="Nine"; 
           break;
      }
    }
    //if dice one is 4
    switch(dice1){
      case 4: switch(dice2){
         case 1: score="Fever Five";
           break;
         case 2: score="Easy Six"; 
           break;
         case 3: score="Seven Out";
           break;
         case 4: score="Hard Eight"; 
           break;
         case 5: score="Nine";
           break;
         case 6: score="Easy Ten"; 
           break;
       }
     }
    //if dice one is 5
    switch(dice1){
      case 5: switch(dice2){
         case 1: score="Easy Six";
            break;
         case 2: score="Seven Out"; 
            break;
         case 3: score="Easy Eight";
            break;
         case 4: score="Nine"; 
            break;
         case 5: score="Hard Ten";
            break;
         case 6: score="Yo-leven"; 
            break;
        }
      }
    //if dice one is 6
    switch(dice1){
      case 6: switch(dice2){
         case 1: score="Seven Out";
            break;
         case 2: score="Easy Eight"; 
            break;
         case 3: score="Nine";
            break;
         case 4: score="Easy Ten"; 
            break;
         case 5: score="Yo-leven";
            break;
         case 6: score="Box Cars"; 
            break;
         }
       }  
    
     //prints out the dice rolled and the score  
    System.out.println("Your numbers were " +dice1+ " and " +dice2+ ".");
    System.out.println(score);
      
  }
}