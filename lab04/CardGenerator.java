//Antonio Lia
//Lab 04
//9-20-2018
//This is a program that will pick a random card from a deck.
//It generates a random number from 1-52.



import java.util.Random;

public class CardGenerator{
  public static void main(String[] args){
    Random randGen=new Random();
    
    int myNumber=(int)((Math.random()*52)+1);//picks random number from 1 to 52
    int myCardVal=myNumber%13;//initiates variablde for card number
    String mySuitName="null";//initiates variable for the suit name
    int mySuit=myNumber/13;//creates a value for the suit of the cards
    
    //Generates the type of suit for the card. 
    if(mySuit<=1){
      mySuitName="Diamonds";
    }
    else if(1<mySuit && mySuit<=2){
      mySuitName="Clubs";
    }
    else if(2<mySuit && mySuit<=3){
      mySuitName="Hearts";
    }
    else if(3<mySuit && mySuit<=4){  
      mySuitName="Spades";   
    }
    
    //assigns a number value to the card
    switch(myCardVal){
      case 1: System.out.println("Ace");
        break;
      case 2: System.out.println("2");
        break;
      case 3: System.out.println("3");
        break;
      case 4: System.out.println("4");
        break;
       case 5: System.out.println("5");
        break;
      case 6: System.out.println("6");
        break;
      case 7: System.out.println("7");
        break;
      case 8: System.out.println("8");
        break;
      case 9: System.out.println("9");
        break;
      case 10: System.out.println("10");
        break;
      case 11: System.out.println("Jack");
        break;
      case 12: System.out.println("Queen");
        break;
      case 13: System.out.println("King");
        break;   
    }
      
      
    //prints out what card is picked
    System.out.println("You picked the " + myCardVal + " of " + mySuitName + ".");
    
  }
}