//imports the scanner utility in the program
import java.util.Scanner;

//Antonio Lia
//09/13/2018
//CSE 002-310
//This program will take a bill and split it between people
//by implementing the scanner function in Java. It will ask the
//percentage tip and how many ways it will be split.

public class Check{
//main method required for   
public static void main(String[] args){
  Scanner myScanner = new Scanner( System.in ); //creates a new scanner
  System.out.print("Enter the original cost of the check in the form xx.xx: ");//asks the user to input the amount for the check
  double checkCost = myScanner.nextDouble();//allows the user to input the check price
   
  System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " );//asks the user for the tip amount
  double tipPercent = myScanner.nextDouble();//allows the user to input the tip percentage
  tipPercent /= 100; //We want to convert the percentage into a decimal value
  
  System.out.print("Enter the number of people who went out to dinner: " );//asks the user how many people went out to eat
  int numPeople = myScanner.nextInt();//allows the user to input the amount ofpeople who went out to eat
  
  double totalCost;
  double costPerPerson;
  int dollars,   //whole dollar amount of cost 
      dimes,
      pennies; //for storing digits
                //to the right of the decimal point 
                //for the cost$ 
  totalCost = checkCost * (1 + tipPercent);
  costPerPerson = totalCost / numPeople; //get the whole amount, dropping decimal fraction
  dollars=(int)costPerPerson;
  //get dimes amount, e.g., 
  // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
  //  where the % (mod) operator returns the remainder
  //  after the division:   583%100 -> 83, 27%5 -> 2 
  dimes=(int)(costPerPerson * 10) % 10;
  pennies=(int)(costPerPerson * 100) % 10;
  System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);

  
  
}//end of main method  
}//end of class 