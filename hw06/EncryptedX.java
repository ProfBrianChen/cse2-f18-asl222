import java.util.Scanner;

/**
 * This program creates a specific pattern from a 
 * given user input that makes an X shape.
 * 
 * Antonio Lia
 * HW 06
 * 10/23/18
 */


public class EncryptedX{
  public static void main(String[] args){
    
    Scanner myScanner=new Scanner(System.in);//creates the scanner
        int input=0;//initiates input variable
        System.out.println("Enter the number of rows you would like:");//asks user for how many rows they would like
      
      //checks to make sure if proper variable 
        while(!myScanner.hasNextInt()){
            myScanner.next();
            System.out.println("Invalid input. Try Again.");
          }
    
        input=myScanner.nextInt();
        
        while(input<0 || input>100){
          System.out.println("Invalid input. Try Again.");
          input=myScanner.nextInt();
        }
    
        int space=0;//creates a variable to make spaces
    
        //nested for-loop that cause the program to print an x-shape 
        for(int rows=1; rows<=input; rows++){
            System.out.println("");
          for(int stars=1; stars<=input; stars++){
            if(stars==rows || stars==input-space){
              System.out.print(" ");
            }
            else{
              System.out.print("*");
              }
            }
            space++;
          }
    
  }
}