import java.util.Random;
import java.util.Scanner;

public class Poker{
    public static void main(String[] args){
        
        Scanner myScanner=new Scanner(System.in);//creates scanner

      //initiates all variables
        int hands=0;
        int fourOfAKind=0;
        int threeOfAKind=0;
        int twoPairs=0; 
        int onePair=0;
        int fullHouse=0;
        String card1SuitName="null";
        String card2SuitName="null";
        String card3SuitName="null";
        String card4SuitName="null";
        String card5SuitName="null";
        int numOfPlays=0;
        int card1=0;
        int card2=0;
        int card3=0;
        int card4=0;
        int card5=0;

      
        System.out.println("Enter the number of hands you would like to play.");//asks how many hands would like to be played
      
      //makes sure proper variable is entered
        while(!myScanner.hasNextInt()){
            myScanner.next();
            System.out.println("Invalid input. Try Again.");
        }
        hands=myScanner.nextInt();

      //makes sure no cards are the same and that they are numbers between 1 and 52
        while(numOfPlays<hands){
            card1=(int)((Math.random()*52)+1);
            card2=(int)((Math.random()*52)+1);
            card3=(int)((Math.random()*52)+1);
            card4=(int)((Math.random()*52)+1);
            card5=(int)((Math.random()*52)+1);

            while(card1==card2){
                card2=(int)((Math.random()*52)+1);
            }

            //card3=(int)((Math.random()*52)+1);

            while(card3==card2 || card3==card1){
                card3=(int)((Math.random()*52)+1);
            }

            //card4=(int)((Math.random()*52)+1); 

            while(card4==card3|| card4==card2 || card4==card1){
                card4=(int)((Math.random()*52)+1);
            }

       
            while(card5==card4 || card5==card3|| card5==card2 || card5==card1){
                card5=(int)((Math.random()*52)+1);
            }

            int card1Val=card1%13;//initiates variable for card number
            int card2Val=card2%13;//initiates variable for card number
            int card3Val=card3%13;//initiates variable for card number
            int card4Val=card4%13;//initiates variable for card number
            int card5Val=card5%13;//initiates variable for card number

          //gives value to the cards from 1 to 5
            switch(card1Val){
                case 0: System.out.println("King");
                break;  
                case 1: System.out.println("Ace");
                break;
                case 2: System.out.println("2");
                break;
                case 3: System.out.println("3");
                break;
                case 4: System.out.println("4");
                break;
                case 5: System.out.println("5");
                break;
                case 6: System.out.println("6");
                break;
                case 7: System.out.println("7");
                break;
                case 8: System.out.println("8");
                break;
                case 9: System.out.println("9");
                break;
                case 10: System.out.println("10");
                break;
                case 11: System.out.println("Jack");
                break;
                case 12: System.out.println("Queen");
                break; 
            }

            switch(card2Val){
                case 0: System.out.println("King");
                break;  
                case 1: System.out.println("Ace");
                break;
                case 2: System.out.println("2");
                break;
                case 3: System.out.println("3");
                break;
                case 4: System.out.println("4");
                break;
                case 5: System.out.println("5");
                break;
                case 6: System.out.println("6");
                break;
                case 7: System.out.println("7");
                break;
                case 8: System.out.println("8");
                break;
                case 9: System.out.println("9");
                break;
                case 10: System.out.println("10");
                break;
                case 11: System.out.println("Jack");
                break;
                case 12: System.out.println("Queen");
                break;
                case 13: System.out.println("King");
                break;   
            }

            switch(card3Val){
                case 0: System.out.println("King");
                break;  
                case 1: System.out.println("Ace");
                break;
                case 2: System.out.println("2");
                break;
                case 3: System.out.println("3");
                break;
                case 4: System.out.println("4");
                break;
                case 5: System.out.println("5");
                break;
                case 6: System.out.println("6");
                break;
                case 7: System.out.println("7");
                break;
                case 8: System.out.println("8");
                break;
                case 9: System.out.println("9");
                break;
                case 10: System.out.println("10");
                break;
                case 11: System.out.println("Jack");
                break;
                case 12: System.out.println("Queen");
                break; 
            }

            switch(card4Val){
                case 0: System.out.println("King");
                break;  
                case 1: System.out.println("Ace");
                break;
                case 2: System.out.println("2");
                break;
                case 3: System.out.println("3");
                break;
                case 4: System.out.println("4");
                break;
                case 5: System.out.println("5");
                break;
                case 6: System.out.println("6");
                break;
                case 7: System.out.println("7");
                break;
                case 8: System.out.println("8");
                break;
                case 9: System.out.println("9");
                break;
                case 10: System.out.println("10");
                break;
                case 11: System.out.println("Jack");
                break;
                case 12: System.out.println("Queen");
                break;  
            }

            switch(card5Val){
                case 0: System.out.println("King");
                break;  
                case 1: System.out.println("Ace");
                break;
                case 2: System.out.println("2");
                break;
                case 3: System.out.println("3");
                break;
                case 4: System.out.println("4");
                break;
                case 5: System.out.println("5");
                break;
                case 6: System.out.println("6");
                break;
                case 7: System.out.println("7");
                break;
                case 8: System.out.println("8");
                break;
                case 9: System.out.println("9");
                break;
                case 10: System.out.println("10");
                break;
                case 11: System.out.println("Jack");
                break;
                case 12: System.out.println("Queen");
                break;  
            }
          
            //gives suit value to the cards
            if(card1/13<=1){
                card1SuitName="Diamonds";
            }
            else if(1<card1/13 && card1/13<=2){
                card1SuitName="Clubs";
            }
            else if(2<card1/13 && card1/13<=3){
                card1SuitName="Hearts";
            }
            else if(3<card1/13 && card1/13<=4){  
                card1SuitName="Spades";   
            }

            if(card2/13<=1){
                card2SuitName="Diamonds";
            }
            else if(1<card2/13 && card2/13<=2){
                card2SuitName="Clubs";
            }
            else if(2<card2/13 && card2/13<=3){
                card2SuitName="Hearts";
            }
            else if(3<card2/13 && card2/13<=4){  
                card2SuitName="Spades";   
            }

            if(card3/13<=1){
                card3SuitName="Diamonds";
            }
            else if(1<card3/13 && card3/13<=2){
                card3SuitName="Clubs";
            }
            else if(2<card3/13 && card3/13<=3){
                card3SuitName="Hearts";
            }
            else if(3<card3/13 && card3/13<=4){  
                card3SuitName="Spades";   
            }

            if(card4/13<=1){
                card4SuitName="Diamonds";
            }
            else if(1<card4/13 && card4/13<=2){
                card4SuitName="Clubs";
            }
            else if(2<card4/13 && card4/13<=3){
                card4SuitName="Hearts";
            }
            else if(3<card4/13 && card4/13<=4){  
                card4SuitName="Spades";   
            }

            if(card5/13<=1){
                card5SuitName="Diamonds";
            }
            else if(1<card5/13 && card5/13<=2){
                card5SuitName="Clubs";
            }
            else if(2<card5/13 && card5/13<=3){
                card5SuitName="Hearts";
            }
            else if(3<card5/13 && card5/13<=4){  
                card5SuitName="Spades";   
            }

          //prints the value and suit of the card
            System.out.println(card1Val+ " of " +card1SuitName+ ", " +card2Val+" of " +card2SuitName+ ", " +card3Val+ " of "+card3SuitName+", " +card4Val+" of "+card4SuitName+", " +card5Val+" of "+card5SuitName);

          //checks what kind of hand is dealt
            if(card1Val==card2Val && card1Val==card3Val && card1Val==card4Val ||
            card1Val==card2Val && card1Val==card3Val && card1Val==card5Val ||
            card1Val==card3Val && card1Val==card4Val && card1Val==card5Val ||
            card1Val==card2Val && card1Val==card4Val && card1Val==card5Val ||
            card2Val==card3Val && card2Val==card4Val && card2Val==card5Val){

                System.out.println("Four of a kind.");
                fourOfAKind++;
            }

            else if(card1Val==card2Val && card1Val==card3Val ||
            card1Val==card2Val && card1Val==card4Val ||
            card1Val==card2Val && card1Val==card5Val ||
            card1Val==card3Val && card1Val==card4Val ||
            card1Val==card3Val && card1Val==card5Val ||
            card1Val==card4Val && card1Val==card5Val ||
            card2Val==card3Val && card2Val==card4Val ||
            card2Val==card3Val && card2Val==card5Val ||
            card2Val==card4Val && card2Val==card5Val ||
            card3Val==card4Val && card3Val==card5Val){
                System.out.println("Three of a Kind.");
                threeOfAKind++;
            }
            else if(card1Val==card2Val && card1Val==card3Val && card4Val==card5Val||
                   card1Val==card2Val && card1Val==card4Val && card3Val==card5Val ||
                   card1Val==card2Val && card1Val==card5Val && card3Val==card4Val||
                   card1Val==card3Val && card1Val==card4Val && card2Val==card5Val||
                   card1Val==card3Val && card1Val==card5Val && card2Val==card4Val||
                   card1Val==card4Val && card1Val==card5Val && card2Val==card3Val||
                   card2Val==card3Val && card2Val==card4Val && card1Val==card5Val||
                   card2Val==card3Val && card2Val==card5Val && card1Val==card4Val||
                   card2Val==card4Val && card2Val==card5Val && card1Val==card3Val||
                   card3Val==card4Val && card3Val==card5Val && card1Val==card2Val){
                System.out.println("Full House.");
                fullHouse++;
            }

            else if(card1Val==card2Val && card3Val==card4Val ||
            card1Val==card2Val && card3Val==card5Val ||
            card1Val==card2Val && card4Val==card5Val ||
            card1Val==card3Val && card2Val==card4Val ||
            card1Val==card3Val && card2Val==card5Val ||
            card1Val==card4Val && card2Val==card3Val ||
            card1Val==card4Val && card2Val==card5Val ||
            card1Val==card5Val && card2Val==card3Val ||
            card1Val==card5Val && card2Val==card4Val ||
            card2Val==card3Val && card4Val==card5Val ||
            card2Val==card4Val && card3Val==card5Val ||
            card2Val==card5Val && card3Val==card4Val){
                System.out.println("Two Pairs.");
                twoPairs++;
            }

            else if(card1Val==card2Val || 
            card1Val==card3Val ||
            card1Val==card4Val || 
            card1Val==card5Val ||
            card2Val==card3Val || 
            card2Val==card4Val ||
            card2Val==card5Val || 
            card3Val==card4Val ||
            card3Val==card5Val || 
            card4Val==card5Val){
                System.out.println("One Pair.");
                onePair++;
            }

            ++numOfPlays;

        }
      
      //finds the percentage likelihood of each card
        double avgFourOfAKind=(double)fourOfAKind/(double)hands;
        double avgThreeOfAKind=(double)threeOfAKind/(double)hands;
        double avgTwoPairs=(double)twoPairs/(double)hands;
        double avgOnePair=(double)onePair/(double)hands;
        double avgFullHouse=(double)fullHouse/(double)hands;
       
      //prints how many hands are played and the probablity of each occurring
        System.out.println("The number of loops: " + hands);
        System.out.println("Probability of Four of a Kind: "+avgFourOfAKind);
        System.out.println("Probability of Three of a Kind: "+avgThreeOfAKind);
        System.out.println("Probability of Full House: "+avgFullHouse);
        System.out.println("Probability of Two Pairs: " +avgTwoPairs);
        System.out.println("Probability of One Pair: "+avgOnePair);
    }
}