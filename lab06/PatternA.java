import java.util.Scanner;//imports the scanner
/**
 * This program creates a specific pattern from a 
 * given user input.
 *
 * Antonio Lia
 * Lab 06
 */
public class PatternA//states the class name
{
    public static void main(String[] args){
        
        Scanner myScanner=new Scanner(System.in);//creates the scanner
        int input=0;//declares iniput variable
        System.out.println("Enter the number of rows you would like:");//asks the user to input a certain number of rows
        
        //checks to make sure the user inputs the correct variable type
        while(!myScanner.hasNextInt()){
            myScanner.next();
            System.out.println("Invalid input. Try Again.");
        }
        input=myScanner.nextInt();
       
        //prints the pattern in given directions      
        for(int numRows=1; numRows<=input; numRows++){
            System.out.println("");
            for(int counter=0; counter<numRows; counter++){
               System.out.print(numRows); 
               System.out.print(" ");
            }
        }
    }
}
