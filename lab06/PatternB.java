import java.util.Scanner;
/**
 * This program creates a specific pattern from a 
 * given user input.
 *
 * Antonio Lia
 * Lab 06
 */
public class PatternB
{
    public static void main(String[] args){
        
        Scanner myScanner=new Scanner(System.in);//declares the scanner
        int input=0;//initiates input variable
        System.out.println("Enter the number of rows you would like:");//asks the user how many rows they would like
       
        //checks to make sure the variable is correct
        while(!myScanner.hasNextInt()){
            myScanner.next();
            System.out.println("Invalid input. Try Again.");
        }
        input=myScanner.nextInt();
       
      //prints the pattern from directions
        for(int numRows=input; numRows>0; numRows--){
            System.out.println("");
            for(int counter=numRows; counter>0; counter--){
               System.out.print(numRows); 
               System.out.print(" ");
            }
        }
    }
}
