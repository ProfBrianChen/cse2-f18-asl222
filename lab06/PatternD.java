import java.util.Scanner;
/**
 * This program creates a specific pattern from a 
 * given user input.
 * 
 * Antonio Lia
 * Lab 06
 */
public class PatternD
{
    public static void main(String[] args){
        
        Scanner myScanner=new Scanner(System.in);//creates the scanner
        int input=0;//initiates input variable
        System.out.println("Enter the number of rows you would like:");//asks user for how many rows they would like
      
      //checks to make sure if proper variable
        while(!myScanner.hasNextInt()){
            myScanner.next();
            System.out.println("Invalid input. Try Again.");
        }
        input=myScanner.nextInt();
       
      //prints pattern from directions
        for(int numRows=input; numRows>0; numRows--){
            System.out.println("");
            for(int counter=numRows+1; counter>1; counter--){
               System.out.print(counter-1); 
               
               System.out.print(" ");
            }
        }
    }
}
