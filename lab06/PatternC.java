import java.util.Scanner;
/**
 * This program creates a specific pattern from a 
 * given user input.
 * 
 * Antonio Lia
 * Lab 06
 */
public class PatternC
{
    public static void main(String[] args){
        
        Scanner myScanner=new Scanner(System.in);//creates the scanner
        int input=0;//initiates input variable
        System.out.println("Enter the number of rows you would like:");//asks the user for how many rows
      
      //checks to make sure variable is correct
        while(!myScanner.hasNextInt()){
            myScanner.next();
            System.out.println("Invalid input. Try Again.");
        }
        input=myScanner.nextInt();

        System.out.println(" ");

      //prints the pattern from directions
        for(int numRows=1; numRows<=input; numRows++){
            System.out.println("");
            for(int spaces=input-numRows; spaces>=0; spaces--){
                System.out.print(" ");
            }
            for(int counter=numRows+1; counter>1; counter--){
                    System.out.print(counter-1); 
                }
        }
    }   
}
