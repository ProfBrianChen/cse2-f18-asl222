//Antonio Lia
//This program creates a tic-tac-toe
//board using arrays. The user inputs a row
//and column integer corresponding to the array
//and will cause an X or O to be printed out, 
//depending on whose turn it is. 

import java.util.*;

public class TicTacToe{
  public static void main(String[] args){
    Scanner myScan = new Scanner(System.in);
    String[][] board = {{"1", "2", "3"},
                        {"4", "5", "6"},
                        {"7", "8", "9"}
                        };
    String userX="X";
    String userO="O";
    int userRowInput=0;
    int userColumnInput=0;
    boolean playing=true;
    boolean haveWinner=false;
    boolean xTurn=true;
    boolean oTurn=false;
    int count=0;
    print2DArray(board);
    //keeps the user plaing until the game is over or there is a winner
    while(playing==true){
      //runs when while it is x's turn to make a move and input a row
      while(xTurn==true){
        System.out.print("It is X's turn. Enter the row you would like to go to (1-3): ");
        //makes sure the user inputs an integer
        while(!myScan.hasNextInt()){
            myScan.next();
            System.out.println("Not an integer. Try Again.");
          }
        userRowInput=myScan.nextInt();
        //makes sure integer is within bounds
        while(userRowInput<1 || userRowInput>board.length){
            System.out.println("Not within bound. Try Again.");
            userRowInput=myScan.nextInt();
          }
        //causes the scanner to ask for an input for the column number
        System.out.print("It is still X's turn. Enter the column you would like to go to: ");
        //makes sure user is printing out an integer
        while(!myScan.hasNextInt()){
            myScan.next();
            System.out.println("Not an integer. Try Again.");
          }
        userColumnInput=myScan.nextInt();
        //makes sure user is within bound
        while(userColumnInput<1 || userColumnInput>board.length){
            System.out.println("Not within bound. Try Again.");
            userColumnInput=myScan.nextInt();
          }
        //makes the user redo it's turn if the spot is selected
        if(board[userRowInput-1][userColumnInput-1]==userX || board[userRowInput-1][userColumnInput-1]==userO){
          System.out.println("Spot already selected. Try Again.");
          continue;
          }
        //prints out "X"
        else{
           board[userRowInput-1][userColumnInput-1]="X";
           xTurn=false;
           oTurn=true;
        }
      }
      haveWinner=checkGameOver(board);//checks if x wins the game
      if(haveWinner==true){
        playing=false;
        break;
      }
      count++;
      //checks to see if there is a tie
      if(playing==true && count==9){
        System.out.println("It's a tie.");
        playing=false;
        break;
      }
      print2DArray(board);//prints the edited board
      //keeps O's turn valid in this loop
      while(oTurn==true){
      //asks user to input a row value for O
      System.out.print("It is O's turn. Enter the row you would like to go to(1-3): ");
      //makes sure user is printing out an integer
      while(!myScan.hasNextInt()){
          myScan.next();
          System.out.println("Not an integer. Try Again.");
        }
      userRowInput=myScan.nextInt();
      //makes sure integer is within bound
      while(userRowInput<1 || userRowInput>board.length){
          System.out.println("Not within bound. Try Again.");
          userRowInput=myScan.nextInt();
        }
      //asks user to print out a column integer
      System.out.print("It is still O's turn. Enter the column you would like to go to: ");
      //makes sure value is an integer
      while(!myScan.hasNextInt()){
          myScan.next();
          System.out.println("Not an integer. Try Again.");
        }
      userColumnInput=myScan.nextInt();
      //makes sure user is within bound
      while(userColumnInput<1 || userColumnInput>board.length){
          System.out.println("Not within bound. Try Again.");
          userColumnInput=myScan.nextInt();
        }
      //makes the user go again if the spot is already selected
      if(board[userRowInput-1][userColumnInput-1]==userX || board[userRowInput-1][userColumnInput-1]==userO){
          System.out.println("Spot already selected. Try Again.");
          continue;
        }
      else{
          board[userRowInput-1][userColumnInput-1]="O";
          xTurn=true;
          oTurn=false;
        }
      }
      haveWinner=checkGameOver(board);//checks if O wins the game
      if(haveWinner==true){
        playing=false;
        break;
      }
      count++;
      print2DArray(board);//updates the board
    }
  }
  
  //methd that prints the 2-D array
  public static void print2DArray(String[][] array){
    for(int i=0; i<array.length;i++){
      for(int j=0; j<array[i].length; j++){
        System.out.print(array[i][j]+" ");
      }
      System.out.println();
    }
  }
 
  //checks to see if X wins or O wins
  public static boolean checkGameOver(String[][] array){
    if((array[0][0]=="X" && array[0][1]=="X" && array[0][2]=="X") ||
       (array[1][0]=="X" && array[1][1]=="X" && array[1][2]=="X") ||
       (array[2][0]=="X" && array[2][1]=="X" && array[2][2]=="X") ||
       (array[0][0]=="X" && array[1][0]=="X" && array[2][0]=="X") ||
       (array[0][1]=="X" && array[1][1]=="X" && array[2][1]=="X") ||
       (array[0][2]=="X" && array[1][2]=="X" && array[2][2]=="X") ||
       (array[0][0]=="X" && array[1][1]=="X" && array[2][2]=="X") ||
       (array[0][2]=="X" && array[1][1]=="X" && array[2][0]=="X")){
      System.out.println("X wins!");
      return true;
    }
    else if((array[0][0]=="O" && array[0][1]=="O" && array[0][2]=="O") ||
       (array[1][0]=="O" && array[1][1]=="O" && array[1][2]=="O") ||
       (array[2][0]=="O" && array[2][1]=="O" && array[2][2]=="O") ||
       (array[0][0]=="O" && array[1][0]=="O" && array[2][0]=="O") ||
       (array[0][1]=="O" && array[1][1]=="O" && array[2][1]=="O") ||
       (array[0][2]=="O" && array[1][2]=="O" && array[2][2]=="O") ||
       (array[0][0]=="O" && array[1][1]=="O" && array[2][2]=="O") ||
       (array[0][2]=="O" && array[1][1]=="O" && array[2][0]=="O")){
      System.out.println("O wins!");
      return true;
    }
   return false; 
  } 
  
}
